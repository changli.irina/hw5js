function createNewUser() {
    const firstName = prompt("Введіть ім'я:");
    const lastName = prompt("Введіть прізвище:");
  
    const newUser = {
      firstName: firstName,
      lastName: lastName,
      getLogin: function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
      }
    };
  
    return newUser;
  }
  
  const user = createNewUser();
  console.log(user.getLogin());